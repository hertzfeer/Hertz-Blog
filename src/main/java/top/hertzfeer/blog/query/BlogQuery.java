package top.hertzfeer.blog.query;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author by Hertz
 * @Classname BlogQuery
 * @Description TODO
 * @Date 2020/2/16 13:52
 */
@ToString(exclude = {"blogs"})
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BlogQuery {

    private String title;
    private Long typeId;
    private Boolean recommend =false;

    public Boolean getRecommend() {
        return recommend;
    }

}
