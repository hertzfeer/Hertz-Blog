package top.hertzfeer.blog.config;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import top.hertzfeer.blog.interceptor.LoginInterceptor;

/**
 * @author by Hertz
 * @Classname WebConfig
 * @Description TODO
 * @Date 2020/2/13 15:06
 */
@Component
public class WebConfig implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new LoginInterceptor()).addPathPatterns("/admin/**")
                .excludePathPatterns("/admin","/admin/login","/admin/rollback");
    }

}
