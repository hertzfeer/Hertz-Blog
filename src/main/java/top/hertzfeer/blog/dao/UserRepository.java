package top.hertzfeer.blog.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import top.hertzfeer.blog.pojo.User;

/**
 * @author by Hertz
 * @Classname UserRepository
 * @Description TODO
 * @Date 2020/2/12 21:22
 */
public interface UserRepository extends JpaRepository<User,Long> {

    /**
     * 根据用户名和密码查询
     * @param username
     * @param password
     * @return
     */
    User findByUsernameAndPassword(String username,String password );
}
