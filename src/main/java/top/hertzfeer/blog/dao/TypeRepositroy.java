package top.hertzfeer.blog.dao;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import top.hertzfeer.blog.pojo.Type;

import java.util.List;

/**
 * @author by Hertz
 * @Classname TypeRepositroy
 * @Description TODO
 * @Date 2020/2/13 15:59
 */
public interface TypeRepositroy extends JpaRepository<Type,Long> {

    Type findByName(String name);

    @Query("select t from Type t")
    List<Type> findTop(Pageable pageable);
}
