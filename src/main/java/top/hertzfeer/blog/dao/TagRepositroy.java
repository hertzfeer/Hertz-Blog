package top.hertzfeer.blog.dao;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import top.hertzfeer.blog.pojo.Tag;

import java.util.List;

/**
 * @author by Hertz
 * @Classname TagRepositroy
 * @Description TODO
 * @Date 2020/2/14 15:59
 */
public interface TagRepositroy extends JpaRepository<Tag,Long> , JpaSpecificationExecutor<Tag> {

    Tag findByName(String name);

    @Query("select t from Tag t ")
    List<Tag> findTop(Pageable pageable);
}
