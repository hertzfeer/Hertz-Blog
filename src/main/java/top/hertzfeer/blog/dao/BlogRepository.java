package top.hertzfeer.blog.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
import top.hertzfeer.blog.pojo.Blog;

import java.util.List;

/**
 * @author by Hertz
 * @Classname BlogRepository
 * @Description TODO
 * @Date 2020/2/15 14:56
 */
public interface BlogRepository extends JpaRepository<Blog,Long>, JpaSpecificationExecutor<Blog> {

    @Query("select b from Blog b where b.recommend=true and b.published=true ")
    List<Blog> findTop(Pageable pageable);

    //List<Blog> findAllOrOrderByCreateTimeDesc(Specification specification,Pageable pageable);


    Page<Blog> findByPublishedIsTrue(Pageable pageable);

    @Query("select b from Blog b where b.title like ?1 or b.content like ?1 and b.published=true"  )
    Page<Blog> findQuery(String query,Pageable pageable);

    @Transactional
    @Modifying
    @Query("update Blog b set b.views = b.views+1 where b.id = ?1")
    int updateViews(Long id);

    @Query("select function('date_format',b.updateTime,'%Y') as year from Blog b group by function('date_format',b.updateTime,'%Y') order by year desc")
    List<String> findGroupYear();

    @Query("select b from Blog b where function('date_format',b.updateTime,'%Y') = ?1 and b.published = true")
    List<Blog> findByYear(String year);

    Blog findByIdAndPublishedIsTrue(Long id);

    Long countByPublishedIsTrue();

}
