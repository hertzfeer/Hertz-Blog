package top.hertzfeer.blog.dao;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import top.hertzfeer.blog.pojo.Comment;

import java.util.List;

/**
 * @author by Hertz
 * @Classname CommentRepository
 * @Description TODO
 * @Date 2020/2/22 19:32
 */
public interface CommentRepository extends JpaRepository<Comment,Long> {

    List<Comment> findByBlogIdAndParentCommentNull(Long id, Sort sort);


    void deleteByBlogId(Long id);
}
