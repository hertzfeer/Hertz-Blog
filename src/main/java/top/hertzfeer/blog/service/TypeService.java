package top.hertzfeer.blog.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import top.hertzfeer.blog.pojo.Type;

import java.util.List;

/**
 * @author by Hertz
 * @Classname TypeService
 * @Description TODO
 * @Date 2020/2/13 15:52
 */
public interface TypeService {

    /**
     * 得到所有的type
     * @return
     */
    List<Type> getAllType();

    /**
     * 添加一个类型
     * @param type
     * @return
     */
    Type saveType(Type type);

    /**
     * 查询类型
     * @param id
     * @return
     */
    Type getType(Long id);

    /**
     * 修改类型参数
     * @return
     */
    Type updateType(Type type);

    /**
     * 得到type列表（分页）
     * @param pageable
     * @return
     */
    Page<Type> listType(Pageable pageable);

    /**
     * 删除类型
     * @param id
     */
    void deleteType(Long id);

    /**
     * 根据名称查找
     * @param name
     * @return
     */
    Type getTypeByName(String name);

    /**
     * 得到前几个数据
     * @param size
     * @return
     */
    List<Type> listTypeTop(Integer size);
}
