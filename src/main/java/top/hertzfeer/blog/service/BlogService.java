package top.hertzfeer.blog.service;

import javassist.NotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import top.hertzfeer.blog.pojo.Blog;
import top.hertzfeer.blog.query.BlogQuery;

import java.util.List;
import java.util.Map;

/**
 * @author by Hertz
 * @Classname BlogService
 * @Description TODO
 * @Date 2020/2/15 14:51
 */
public interface BlogService {

    /**
     * 分页查询
     * @param pageable
     * @return
     */
    Page<Blog> listBlog(Pageable pageable);
    /**
     * 根据主键id查询
     * @param id
     * @return
     */
    Blog getBlog(Long id);

    /**
     * 分页 条件 查询Blog
     * @param pageable
     * @return
     */
    Page<Blog> listBlog(Pageable pageable, BlogQuery blogQuery);

    /**
     * 新建Blog
     * @param blog
     * @return
     */
    Blog saveBlog(Blog blog);

    /**
     * 修改Blog
     * @param blog
     * @return
     */
    Blog updateBlog(Blog blog);

    /**
     * 删除Blog
     * @param id
     */
    void deleteBlog(Long id);

    /**
     *  查询
     * @param size
     * @return
     */
    List<Blog> listRecommendBlogTop(Integer size);

    /**
     * 查询所有公开的博客
     * @param pageable
     * @return
     */
    Page<Blog> listBlogPublished(Pageable pageable);

    /**
     * 搜索功能的实现
     * @param query
     * @param pageable
     * @return
     */
    Page<Blog> listBlog(String query,Pageable pageable);

    /**
     * 获取blog并且渲染
     * @param id
     * @return
     */
    Blog getAndConvert(Long id) throws NotFoundException;

    /**
     * 查询所有有该tagId对应的Blog的分页  后台类容 查询所有的
     * @param pageable
     * @param tagId
     * @return
     */
    Page<Blog> listBlog(Pageable pageable,Long tagId);

    /**
     * 按照年份 检索所有的博客
     * @return
     */
    Map<String,List<Blog>> archiveBlog();

    /**
     * 返回所有的博客数目
     * @return
     */
    Long count();
}
