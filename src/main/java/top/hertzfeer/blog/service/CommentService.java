package top.hertzfeer.blog.service;

import top.hertzfeer.blog.pojo.Comment;

import java.util.List;

/**
 * @author by Hertz
 * @Classname CommentService
 * @Description TODO
 * @Date 2020/2/22 19:32
 */
public interface CommentService {

    List<Comment> listCommentByBlogId(Long blogid);

    Comment saveComment(Comment comment);

    Comment getCommentById(Long id);

    void  deleteById(Long id);


}
