package top.hertzfeer.blog.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import top.hertzfeer.blog.pojo.Tag;

import java.util.List;

/**
 * @author by Hertz
 * @Classname TagService
 * @Description TODO
 * @Date 2020/2/14 15:52
 */
public interface TagService {

    /**
     * 添加一个类型
     * @param tag
     * @return
     */
    Tag saveTag(Tag tag);

    /**
     * 查询类型
     * @param id
     * @return
     */
    Tag getTag(Long id);

    /**
     * 修改类型参数
     * @return
     */
    Tag updateTag(Tag tag);

    /**
     * 得到type列表（分页）
     * @param pageable
     * @return
     */
    Page<Tag> listTag(Pageable pageable);

    /**
     *
     * @return
     */
    List<Tag> listTag(String ids);
    /**
     * 删除类型
     * @param id
     */
    void deleteTag(Long id);

    /**
     * 根据名称查找
     * @param name
     * @return
     */
    Tag getTagByName(String name);

    /**
     * 获取所有的tag
     * @return
     */
    List<Tag> getAllTag();

    /**
     * 获取tag
     * @return
     */
    List<Tag> listTagTop(Integer size);
}
