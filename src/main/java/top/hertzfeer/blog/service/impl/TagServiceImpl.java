package top.hertzfeer.blog.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.hertzfeer.blog.dao.TagRepositroy;
import top.hertzfeer.blog.pojo.Tag;
import top.hertzfeer.blog.service.TagService;

import java.util.ArrayList;
import java.util.List;

/**
 * @author by Hertz
 * @Classname TagServiceImpl
 * @Description TODO
 * @Date 2020/2/14 19:30
 */
@Service
public class TagServiceImpl implements TagService {

    @Autowired
    private TagRepositroy tagRepositroy;

    @Transactional
    @Override
    public Tag saveTag(Tag tag) {
        return tagRepositroy.save(tag);
    }

    @Transactional
    @Override
    public Tag getTag(Long id) {
        return tagRepositroy.findById(id).get();
    }

    @Transactional
    @Override
    public Tag updateTag(Tag tag) {
        return tagRepositroy.save(tag);
    }

    @Transactional
    @Override
    public List<Tag> listTag(String ids) {
        List<Long> idList = stringToList(ids);
        return tagRepositroy.findAllById(idList);
    }

    @Transactional
    @Override
    public Page<Tag> listTag(Pageable pageable) {
        return tagRepositroy.findAll(pageable);
    }

    @Transactional
    @Override
    public List<Tag> getAllTag() {
        return tagRepositroy.findAll();
    }

    @Override
    public List<Tag> listTagTop(Integer size) {
        Sort sort = Sort.by(Sort.Direction.DESC,"blogs.size");
        Pageable pageable = PageRequest.of(0,size,sort);
        return tagRepositroy.findTop(pageable);
    }

    @Transactional
    @Override
    public void deleteTag(Long id) {
        tagRepositroy.deleteById(id);
    }

    @Transactional
    @Override
    public Tag getTagByName(String name) {
        return tagRepositroy.findByName(name);
    }

    private List<Long> stringToList(String str){
        List<Long> list = new ArrayList<>();
        if(str != null && !"".equals(str)){
            String[] split = str.split(",");
            for(int i = 0; i < split.length ; i++){
                Long id = new Long(split[i]);
                list.add(id);
            }
        }
        return list;
    }
}
