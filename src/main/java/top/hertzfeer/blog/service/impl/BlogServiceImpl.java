package top.hertzfeer.blog.service.impl;

import javassist.NotFoundException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.hertzfeer.blog.dao.BlogRepository;
import top.hertzfeer.blog.dao.CommentRepository;
import top.hertzfeer.blog.pojo.Blog;
import top.hertzfeer.blog.pojo.Type;
import top.hertzfeer.blog.query.BlogQuery;
import top.hertzfeer.blog.service.BlogService;
import top.hertzfeer.blog.util.MarkdownUtils;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author by Hertz
 * @Classname BlogServiceImpl
 * @Description TODO
 * @Date 2020/2/15 14:56
 */
@Service
public class BlogServiceImpl implements BlogService {

    @Autowired
    private BlogRepository blogRepository;

    @Autowired
    private CommentRepository commentRepository;

    @Override
    public Page<Blog> listBlog(String query, Pageable pageable) {
        return blogRepository.findQuery(query,pageable);
    }

    @Override
    public Long count() {
        return blogRepository.countByPublishedIsTrue();
    }

    @Override
    public Map<String, List<Blog>> archiveBlog() {
        Map<String,List<Blog>> map = new HashMap<>();
        List<String> years = blogRepository.findGroupYear();
        for (String year: years) {
            List<Blog> byYear = blogRepository.findByYear(year);
            map.put(year,byYear);
        }
        return map;
    }

    @Override
    public Page<Blog> listBlog(Pageable pageable, Long tagId) {
        return blogRepository.findAll(new Specification<Blog>() {
            @Override
            public Predicate toPredicate(Root<Blog> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                Join join = root.join("tags");
                return criteriaBuilder.equal(join.get("id"),tagId);
            }
        },pageable);
    }

    @Transactional
    @Override
    public Blog getAndConvert(Long id) throws NotFoundException{
        Blog blog = blogRepository.findByIdAndPublishedIsTrue(id);
        Blog coppy = new Blog();
        if(blog == null){
             throw  new NotFoundException("博客不存在！！！");
        }else {
            BeanUtils.copyProperties(blog,coppy);
            String content = coppy.getContent();
            String html = MarkdownUtils.markdownToHtmlExtensions(content);
            blogRepository.updateViews(id);
            coppy.setContent(html);
        }
        return coppy;
    }

    @Override
    public Page<Blog> listBlogPublished(Pageable pageable) {
        return blogRepository.findByPublishedIsTrue(pageable);
    }

    @Override
    public Page<Blog> listBlog(Pageable pageable) {
        return blogRepository.findAll(pageable);
    }

    @Override
    public List<Blog> listRecommendBlogTop(Integer size) {
        Sort sort = Sort.by(Sort.Direction.DESC,"updateTime");
        Pageable pageable = PageRequest.of(0,size,sort);
        return blogRepository.findTop(pageable);
    }

    @Override
    public Blog getBlog(Long id) {
        return blogRepository.findById(id).get();
    }

    @Override
    public Page<Blog> listBlog(Pageable pageable, BlogQuery blog) {
        Specification<Blog> specification = new Specification<Blog>() {
            /**
             *
             * @param root 代表查询的对象是什么，可以获得表的字段
             * @param criteriaQuery  条件容器
             * @param criteriaBuilder  设置具体查询格式 如相对 模糊查询……
             * @return
             */
            @Override
            public Predicate toPredicate(Root<Blog> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<>();
                if (blog.getTitle() != null && !"".equals(blog.getTitle())){
                    predicates.add(criteriaBuilder.like(root.<String>get("title"),"%"+blog.getTitle()+"%"));
                }
                if (blog.getTypeId() != null ){
                    predicates.add(criteriaBuilder.equal(root.<Type>get("type").get("id"),blog.getTypeId()));
                }
                if(blog.getRecommend() ){
                    predicates.add(criteriaBuilder.equal(root.<Boolean>get("commend"),blog.getRecommend()));
                }
                criteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));

                return null;
            }
        };
        return blogRepository.findAll(specification,pageable);
    }

    @Override
    public Blog saveBlog(Blog blog) {
        return blogRepository.save(blog);
    }

    @Override
    public Blog updateBlog(Blog blog) {
        return blogRepository.save(blog);
    }

    @Transactional
    @Override
    public void deleteBlog(Long id) {
        Blog callBack = getBlog(id);
        callBack.setTags(null);
        callBack.setType(null);
        // callBack.setComments(null);
        updateBlog(callBack);
        commentRepository.deleteByBlogId(id);
        blogRepository.deleteById(id);
    }
}
