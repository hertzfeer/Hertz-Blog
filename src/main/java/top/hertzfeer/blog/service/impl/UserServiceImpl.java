package top.hertzfeer.blog.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.hertzfeer.blog.dao.UserRepository;
import top.hertzfeer.blog.pojo.User;
import top.hertzfeer.blog.service.UserService;
import top.hertzfeer.blog.util.MD5Utils;

/**
 * @author by Hertz
 * @Classname UserServiceImpl
 * @Description TODO
 * @Date 2020/2/12 21:12
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public User checkUser(String username, String password) {
        return userRepository.findByUsernameAndPassword(username, MD5Utils.code(password));
    }
}
