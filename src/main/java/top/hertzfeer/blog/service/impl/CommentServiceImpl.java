package top.hertzfeer.blog.service.impl;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.hertzfeer.blog.dao.CommentRepository;
import top.hertzfeer.blog.pojo.Comment;
import top.hertzfeer.blog.service.CommentService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author by Hertz
 * @Classname CommentServiceImpl
 * @Description TODO
 * @Date 2020/2/22 19:32
 */
@Service
public class CommentServiceImpl implements CommentService {


    @Override
    public void deleteById(Long id) {
        commentRepository.deleteByBlogId(id);
    }

    @Autowired
    private CommentRepository commentRepository;

    @Override
    public Comment getCommentById(Long id) {
        return commentRepository.findById(id).get();
    }

    @Override
    public List<Comment> listCommentByBlogId(Long blogid) {
        Sort sort = Sort.by(Sort.Direction.DESC,"createTime");
        List<Comment> listComment = commentRepository.findByBlogIdAndParentCommentNull(blogid, sort);
        return eachComment(listComment);
    }

    @Transactional
    @Override
    public Comment saveComment(Comment comment) {
        Date date = new Date();
        Long parentId = comment.getParentComment().getId();
        if(parentId != -1){
            Comment callBack = commentRepository.findById(parentId).get();
            comment.setParentComment(callBack);
        }else{
            comment.setParentComment(null);
        }
        comment.setCreateTime(date);
        return commentRepository.save(comment);
    }

    /**
     * 循环每个顶级的评论节点
     * @param comments
     * @return
     */
    private List<Comment> eachComment(List<Comment> comments) {
        List<Comment> commentsView = new ArrayList<>();
        for (Comment comment : comments) {
            Comment c = new Comment();
            BeanUtils.copyProperties(comment,c);
            commentsView.add(c);
        }
        //合并评论的各层子代到第一级子代集合中
        combineChildren(commentsView);
        return commentsView;
    }

    /**
     *
     * @param comments root根节点，blog不为空的对象集合
     * @return
     */
    private void combineChildren(List<Comment> comments) {

        for (Comment comment : comments) {
            List<Comment> replys = comment.getReplyComments();
            for(Comment reply : replys) {
                //循环迭代，找出子代，存放在tempReplys中
                recursively(reply);
            }
            //修改顶级节点的reply集合为迭代处理后的集合
            comment.setReplyComments(tempReplys);
            //清除临时存放区
            tempReplys = new ArrayList<>();
        }
    }

    //存放迭代找出的所有子代的集合
    private List<Comment> tempReplys = new ArrayList<>();
    /**
     * 递归迭代，剥洋葱
     * @param comment 被迭代的对象
     * @return
     */
    private void recursively(Comment comment) {
        //顶节点添加到临时存放集合
        tempReplys.add(comment);
        if (comment.getReplyComments().size()>0) {
            List<Comment> replys = comment.getReplyComments();
            for (Comment reply : replys) {
                tempReplys.add(reply);
                if (reply.getReplyComments().size()>0) {
                    recursively(reply);
                }
            }
        }
    }
}
