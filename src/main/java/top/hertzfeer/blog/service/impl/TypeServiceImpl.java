package top.hertzfeer.blog.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.hertzfeer.blog.dao.TypeRepositroy;
import top.hertzfeer.blog.pojo.Type;
import top.hertzfeer.blog.service.TypeService;

import java.util.List;

/**
 * @author by Hertz
 * @Classname TypeServiceImpl
 * @Description TODO
 * @Date 2020/2/13 15:58
 */
@Service
public class TypeServiceImpl implements TypeService {

    @Autowired
    private TypeRepositroy typeRepositroy;

    @Transactional
    @Override
    public Type saveType(Type type) {
        return typeRepositroy.save(type);
    }

    @Transactional
    @Override
    public Type getType(Long id) {
        return typeRepositroy.findById(id).get();
    }

    @Transactional
    @Override
    public Type updateType(Type type) {
        return typeRepositroy.save(type);
    }

    @Override
    public List<Type> getAllType() {
        return typeRepositroy.findAll();
    }

    @Transactional
    @Override
    public Page<Type> listType(Pageable pageable) {
        return typeRepositroy.findAll(pageable);
    }

    @Override
    public List<Type> listTypeTop(Integer size) {
        Sort sort = Sort.by(Sort.Direction.DESC,"blogs.size");
        Pageable pageable = PageRequest.of(0,size,sort);
        return typeRepositroy.findTop(pageable);
    }

    @Transactional
    @Override
    public
    void deleteType(Long id) {
        typeRepositroy.deleteById(id);
    }

    @Override
    public Type getTypeByName(String name) {
        return typeRepositroy.findByName(name);
    }
}
