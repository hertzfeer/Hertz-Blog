package top.hertzfeer.blog.service.impl;

import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import top.hertzfeer.blog.pojo.City;
import top.hertzfeer.blog.pojo.Weather;
import top.hertzfeer.blog.util.GzipUtil;

import java.io.IOException;

/**
 * @author by Hertz
 * @Classname WeacherController
 * @Description TODO
 * @Date 2020/2/25 13:32
 */
@Service
public class TemplateService {

    @Autowired
    private RestTemplate restTemplate ;

    private static final String pk = "lrvxcGmVVyNiEHi6pQj0Aq9tE9BXh9iG";

    public Weather getWeather(String city) throws IOException {
        String result =  restTemplate.getForObject("http://wthrcdn.etouch.cn/weather_mini?city="+city, String.class);
        if(result == null || "".equals(result)){
            return null;
        }else {
            String s = GzipUtil.conventFromGzip(result);
            Weather weather = JSON.parseObject(s, Weather.class);
            return weather;
        }
    }

    public City city (String ip) throws IOException {
        City result = restTemplate.getForObject("http://api.map.baidu.com/location/ip?ak="+pk+"&ip="+ip, City.class);
        //System.out.println(result);
        if(result == null || "".equals(result)){
            return null;
        }else {
            //System.out.println(result.toString());
           // City city = JSON.parseObject(s, City.class);
            return result;
        }
    }

}
