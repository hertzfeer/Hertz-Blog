package top.hertzfeer.blog.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import top.hertzfeer.blog.pojo.Comment;

/**
 * @author by Hertz
 * @Classname MailService
 * @Description TODO
 * @Date 2020/3/8 17:41
 */
@Async
@Service
@Slf4j
public class MailService {

    @Autowired
    private JavaMailSender javaMailSenderImpl;


    private static final String FROM = "767025418@qq.com";

    public void sendMail(String to,String subject,String content){
        SimpleMailMessage s = new SimpleMailMessage();
        s.setFrom(FROM);
        s.setSubject(subject);
        s.setTo(to);
        s.setText(content);
        javaMailSenderImpl.send(s);
    }

    public void doSend(Comment comment,String parentEmail){
        String object = comment.getNickName() + "您的评论的回复";
        try {
            sendMail(parentEmail,object,comment.getContent());
            log.info(comment.getEmail() + "发送给" + parentEmail + "的邮件发送成功");
        } catch (Exception e) {
            log.error(comment.getEmail() + "发送给" + parentEmail + "的邮件发送失败");
            e.printStackTrace();
        }
    }
}
