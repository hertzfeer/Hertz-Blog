package top.hertzfeer.blog.service;

import top.hertzfeer.blog.pojo.User;

/**
 * @author by Hertz
 * @Classname UserService
 * @Description User的业务层
 * @Date 2020/2/12 21:11
 */
public interface UserService {

    /**
     * 用于校验用户的登录信息
     * @param username 用户名
     * @param password 密码
     * @return
     */
    User checkUser(String username, String password);
}
