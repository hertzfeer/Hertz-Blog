package top.hertzfeer.blog.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author by Hertz
 * @Classname AboutController
 * @Description TODO
 * @Date 2020/2/25 20:31
 */
@Controller
public class AboutController {

    @GetMapping("/about")
    public String toAbout(){
        return "about";
    }
}
