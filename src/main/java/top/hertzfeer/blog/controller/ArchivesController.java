package top.hertzfeer.blog.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import top.hertzfeer.blog.service.BlogService;

/**
 * @author by Hertz
 * @Classname ArchivesController
 * @Description TODO
 * @Date 2020/2/25 19:29
 */
@Controller
public class ArchivesController {

    @Autowired
    private BlogService blogServiceImpl;

    @GetMapping("/archivers")
    public String toArchivers(Model model){
        model.addAttribute("blogCount",blogServiceImpl.count());
        model.addAttribute("archiveMap",blogServiceImpl.archiveBlog());
        return "archives";
    }

}
