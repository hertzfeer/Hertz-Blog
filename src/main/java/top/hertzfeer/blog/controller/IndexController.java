package top.hertzfeer.blog.controller;

import com.google.code.kaptcha.Constants;
import com.google.code.kaptcha.Producer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import top.hertzfeer.blog.service.BlogService;
import top.hertzfeer.blog.service.TagService;
import top.hertzfeer.blog.service.TypeService;
import top.hertzfeer.blog.util.IpUtil;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * @author by Hertz
 * @Classname InterController
 * @Description TODO
 * @Date 2020/2/18 19:40
 */
@Controller
public class IndexController {

    @Autowired
    private TagService tagServiceImpl;
    @Autowired
    private BlogService blogServiceImpl;
    @Autowired
    private TypeService typeServiceImpl;
    @Autowired
    private Producer captchaProducer;

    private static Logger logger = LoggerFactory.getLogger(IndexController.class);

    @GetMapping("/")
    public String index(@PageableDefault(size = 8,sort = {"updateTime"} ,direction = Sort.Direction.DESC) Pageable pageable,
                        Model model,
                        HttpServletRequest httpServletRequest){
        model.addAttribute("page",blogServiceImpl.listBlogPublished(pageable));
        model.addAttribute("types",typeServiceImpl.listTypeTop(6));
        model.addAttribute("tags",tagServiceImpl.listTagTop(10));
        model.addAttribute("blogs",blogServiceImpl.listRecommendBlogTop(8));
        String ip = IpUtil.getIpAddr(httpServletRequest);
        logger.info("访问的IP地址为："+ip);
        return "index";
    }

    @RequestMapping("/verificationCode")
    public void getCaptcha(HttpServletResponse response, HttpSession session) throws IOException {

        response.setDateHeader("Expires", 0);

        // Set standard HTTP/1.1 no-cache headers.
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        // Set IE extended HTTP/1.1 no-cache headers (use addHeader).
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        // Set standard HTTP/1.0 no-cache header.
        response.setHeader("Pragma", "no-cache");
        // return a jpeg
        response.setContentType("image/jpeg");
        // create the text for the image
        String capText = captchaProducer.createText();
        logger.info("此次生成的验证码是"+capText);

        // store the text in the session
        //request.getSession().setAttribute(Constants.KAPTCHA_SESSION_KEY, capText);
        //将验证码存到session
        session.setAttribute(Constants.KAPTCHA_SESSION_KEY, capText);
        // create the image with the text
        BufferedImage bi = captchaProducer.createImage(capText);
        ServletOutputStream out = response.getOutputStream();
        // write the data out
        ImageIO.write(bi, "jpg", out);
        try {
            out.flush();
        } finally {
            out.close();
        }

    }


}
