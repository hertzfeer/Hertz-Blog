package top.hertzfeer.blog.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import top.hertzfeer.blog.pojo.User;
import top.hertzfeer.blog.service.UserService;

import javax.servlet.http.HttpSession;

/**
 * @author by Hertz
 * @Classname LoginController
 * @Description TODO
 * @Date 2020/2/12 21:31
 */
@Controller
@RequestMapping("/admin")
public class LoginController {

    @Autowired
    private UserService userServiceImpl;

    @GetMapping
    public String toAdminLogin(){
        return "admin/login";
    }

    @PostMapping("/login")
    public String loginPage(String username,
                            String password,
                            HttpSession session,
                            RedirectAttributes attribute){
        User user = userServiceImpl.checkUser(username, password);
        if(user != null){
            session.setAttribute("user",user);
            return "admin/index";
        }else{
            attribute.addFlashAttribute("message","用户名或者密码错误！！");
            return "redirect:/admin";
        }
    }

    @GetMapping("/logout")
    public String logout(HttpSession session){
            session.removeAttribute("user");
            return "redirect:/admin";
    }

    @GetMapping("/rollback")
    public String rollback(RedirectAttributes attributes){
        attributes.addFlashAttribute("message","请登录！！");
        return "redirect:/admin";
    }
}
