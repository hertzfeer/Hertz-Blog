package top.hertzfeer.blog.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import top.hertzfeer.blog.pojo.Type;
import top.hertzfeer.blog.service.TypeService;

/**
 * @author by Hertz
 * @Classname TypeController
 * @Description TODO
 * @Date 2020/2/13 16:08
 */
@Controller
@RequestMapping("/admin")
public class TypesController {

    @Autowired
    private TypeService typeServiceImpl;

    @GetMapping("/types")
    public String types(@PageableDefault(size = 10 ,sort = {"id"}, direction= Sort.Direction.ASC) Pageable pageable,
                        Model model){
        Page<Type> page = typeServiceImpl.listType(pageable);
        model.addAttribute("page",page);
        return "admin/types";
    }

    @GetMapping("/types/input")
    public String toInput(){
        return "admin/types-input";
    }

    @PutMapping("/types")
    public String inputTypes(Type type, RedirectAttributes attributes){
        if(typeServiceImpl.getTypeByName(type.getName()) != null){
            attributes.addFlashAttribute("error","添加失败,名称已存在！！");
            return "redirect:/admin/types";
        }
        Type callBack = typeServiceImpl.saveType(type);
        if(callBack == null){
            attributes.addFlashAttribute("error","添加失败,分类已存在或者格式异常！！");
            return "redirect:/admin/types";
        }else {
            attributes.addFlashAttribute("message","操作成功!!");
            return "redirect:/admin/types";
        }
    }

    @GetMapping("/types/{id}/update")
    public String toUpdate(@PathVariable Long id,Model model){
        Type type = typeServiceImpl.getType(id);
        model.addAttribute("type",type);
        return "admin/types-update";
    }


    @PostMapping("/types/update")
    public String updateType(Type type,RedirectAttributes attributes ){
        Type callBack = typeServiceImpl.updateType(type);
        if(callBack == null){
            attributes.addFlashAttribute("error","更新失败！！");
            return "redirect:/admin/types";
        }else {
            attributes.addFlashAttribute("message","更新成功!!");
            return "redirect:/admin/types";
        }
    }

    @DeleteMapping("/types/delete/{id}")
    public String deleteType(@PathVariable Long id){
        typeServiceImpl.deleteType(id);
        return "redirect:/admin/types";
    }
}
