package top.hertzfeer.blog.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import top.hertzfeer.blog.pojo.Tag;
import top.hertzfeer.blog.service.TagService;

/**
 * @author by Hertz
 * @Classname TagController
 * @Description TODO
 * @Date 2020/2/13 16:08
 */
@Controller
@RequestMapping("/admin")
public class TagsController {

    @Autowired
    private TagService tagServiceImpl;

    @GetMapping("/tags")
    public String tags(@PageableDefault(size = 10 ,sort = {"id"}, direction= Sort.Direction.ASC) Pageable pageable,
                        Model model){
        Page<Tag> page = tagServiceImpl.listTag(pageable);
        model.addAttribute("page",page);
        return "admin/tags";
    }

    @GetMapping("/tags/input")
    public String toInput(){
        return "admin/tags-input";
    }

    @PutMapping("/tags")
    public String inputTags(Tag tag, RedirectAttributes attributes){
        if(tagServiceImpl.getTagByName(tag.getName()) != null){
            attributes.addFlashAttribute("error","添加失败,名称已存在！！");
            return "redirect:/admin/tags";
        }
        Tag callBack = tagServiceImpl.saveTag(tag);
        if(callBack == null){
            attributes.addFlashAttribute("error","添加失败,分类已存在或者格式异常！！");
            return "redirect:/admin/tags";
        }else {
            attributes.addFlashAttribute("message","操作成功!!");
            return "redirect:/admin/tags";
        }
    }

    @GetMapping("/tags/{id}/update")
    public String toUpdate(@PathVariable Long id,Model model){
        Tag type = tagServiceImpl.getTag(id);
        model.addAttribute("tag",type);
        return "admin/tags-update";
    }


    @PostMapping("/tags/update")
    public String updateType(Tag tag,RedirectAttributes attributes ){
        Tag callBack = tagServiceImpl.updateTag(tag);
        if(callBack == null){
            attributes.addFlashAttribute("error","更新失败！！");
            return "redirect:/admin/tags";
        }else {
            attributes.addFlashAttribute("message","更新成功!!");
            return "redirect:/admin/tags";
        }
    }

    @DeleteMapping("/tags/delete/{id}")
    public String deleteType(@PathVariable Long id){
        tagServiceImpl.deleteTag(id);
        return "redirect:/admin/tags";
    }
}
