package top.hertzfeer.blog.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import top.hertzfeer.blog.pojo.Blog;
import top.hertzfeer.blog.pojo.Tag;
import top.hertzfeer.blog.pojo.Type;
import top.hertzfeer.blog.pojo.User;
import top.hertzfeer.blog.query.BlogQuery;
import top.hertzfeer.blog.service.BlogService;
import top.hertzfeer.blog.service.TagService;
import top.hertzfeer.blog.service.TypeService;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;

/**
 * @author by Hertz
 * @Classname BlogController
 * @Description TODO
 * @Date 2020/2/13 14:43
 */
@Controller
@RequestMapping("/admin")
public class BlogsController {

    @Autowired
    private BlogService blogServiceImpl;

    @Autowired
    private TypeService typeServiceImpl;

    @Autowired
    private TagService tagServiceImpl;


    private static String URL_PREFIX = "https://i.picsum.photos/id/";

    private static String URL_SUFFIX = "/800/600.jpg";

    @GetMapping("/blogs")
    public String blogs(@PageableDefault(size = 8, direction = Sort.Direction.DESC, sort = {"updateTime"}) Pageable pageable,
                        Model model,
                        BlogQuery blogQuery) {
        Page<Blog> page = blogServiceImpl.listBlog(pageable, blogQuery);
        List<Type> types = typeServiceImpl.getAllType();
        model.addAttribute("page", page);
        model.addAttribute("types", types);
        return "admin/blogs";
    }

    @PostMapping("/blogs/search")
    public String serach(@PageableDefault(size = 8, direction = Sort.Direction.DESC, sort = {"updateTime"}) Pageable pageable,
                         Model model,
                         BlogQuery blogQuery) {
        Page<Blog> page = blogServiceImpl.listBlog(pageable, blogQuery);
        model.addAttribute("page", page);
        return "admin/blogs :: blogList";
    }

    @GetMapping("/blogs/input")
    public String toInput(Model model) {
        List<Type> types = typeServiceImpl.getAllType();
        List<Tag> tags = tagServiceImpl.getAllTag();
        model.addAttribute("types", types);
        model.addAttribute("tags", tags);
        return "admin/blogs-input";
    }

    @PutMapping("/blogs")
    public String inputBlogs(Blog blog,
                             RedirectAttributes attributes,
                             Model model,
                             String tagIds,
                             HttpSession session) {
        Date date = new Date();
        blog.setUser((User) session.getAttribute("user"));
        blog.setCreateTime(date);
        blog.setUpdateTime(date);
        String firstPicture = blog.getFirstPicture();
        //生成图片URL
        blog.setFirstPicture(URL_PREFIX+firstPicture+URL_SUFFIX);
        blog.setType(typeServiceImpl.getType(blog.getType().getId()));
        blog.setTags(tagServiceImpl.listTag(tagIds));
        blog.setViews(1);
        Blog callBack = blogServiceImpl.saveBlog(blog);
        if (callBack == null) {
            List<Tag> tags = tagServiceImpl.getAllTag();
            List<Type> types = typeServiceImpl.getAllType();
            model.addAttribute("blog", blog);
            model.addAttribute("tags", tags);
            model.addAttribute("types", types);
            model.addAttribute("error", "发表失败");
            return "admin/blogs-update";
        } else {
            attributes.addFlashAttribute("message", "发表成功！！");
            return "redirect:/admin/blogs";
        }

    }

    @GetMapping("/blogs/{id}/update")
    public String toUpdate(@PathVariable Long id,
                           Model model) {
        Blog blog = blogServiceImpl.getBlog(id);
        List<Tag> tags = blog.getTags();
        String tagIds = Tag.listToString(tags);
        model.addAttribute("blog", blog);
        model.addAttribute("tags", tagServiceImpl.getAllTag());
        model.addAttribute("types", typeServiceImpl.getAllType());
        model.addAttribute("tagIds", tagIds);
        return "admin/blogs-update";
    }

    /**
     * SpringData的多表操作也太麻烦了  这里自己使用了方法
     * @param id
     * @return
     */
    @DeleteMapping("/blogs/delete/{id}")
    public String deleteBlog(@PathVariable Long id) {
        blogServiceImpl.deleteBlog(id);
        return "redirect:/admin/blogs";
    }

    @PostMapping("/blogs")
    public String updateBlog(RedirectAttributes attributes,
                             Model model,
                             String tagIds,
                             Blog blog) {
        Blog oldBlog = blogServiceImpl.getBlog(blog.getId());
        Date date = new Date();
        blog.setId(oldBlog.getId());
        blog.setViews(oldBlog.getViews());
        blog.setCreateTime(oldBlog.getCreateTime());
        blog.setUpdateTime(date);
        blog.setType(typeServiceImpl.getType(blog.getType().getId()));
        blog.setTags(tagServiceImpl.listTag(tagIds));
        blog.setUser(oldBlog.getUser());
        blog.setViews(blog.getViews()+1);
        Blog callBack = blogServiceImpl.updateBlog(blog);
        if (callBack == null) {
            List<Tag> tags = tagServiceImpl.getAllTag();
            List<Type> types = typeServiceImpl.getAllType();
            model.addAttribute("blog", blog);
            model.addAttribute("tags", tags);
            model.addAttribute("types", types);
            model.addAttribute("error", "修改失败");
            return "admin/blogs-update";
        } else {
            attributes.addFlashAttribute("message", "修改成功！！");
            return "redirect:/admin/blogs";
        }
    }

    @GetMapping("/footer/newblog")
    public String newBlogs(Model model){
        model.addAttribute("newblogs",blogServiceImpl.listRecommendBlogTop(3));
        return "admin/_fragments :: newblogList";
    }
}