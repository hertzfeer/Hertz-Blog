package top.hertzfeer.blog.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import top.hertzfeer.blog.pojo.Blog;
import top.hertzfeer.blog.pojo.Tag;
import top.hertzfeer.blog.service.BlogService;
import top.hertzfeer.blog.service.TagService;

import java.util.List;

/**
 * @author by Hertz
 * @Classname TagController
 * @Description TODO
 * @Date 2020/2/24 17:00
 */
@Controller
public class TagController {

    @Autowired
    private TagService tagServiceImpl;

    @Autowired
    private BlogService blogServiceImpl;

    @GetMapping("/tags/{id}")
    public String toTags(@PathVariable Long id,
                         @PageableDefault(size = 8,sort = {"updateTime"} ,direction = Sort.Direction.DESC)Pageable pageable,
                         Model model){
        List<Tag> listTagTop = tagServiceImpl.listTagTop(10000);
        if(listTagTop.size()==0){
            model.addAttribute("page", null);
            model.addAttribute("activeTagId",id);
            model.addAttribute("tags",null);
            return "tags";
        }
        if(id==-1){
            id = listTagTop.get(0).getId();
        }
        Page<Blog> page = blogServiceImpl.listBlog(pageable, id);
        model.addAttribute("page",page);
        model.addAttribute("activeTagId",id);
        model.addAttribute("tags",listTagTop);
        return "tags";
    }
}
