package top.hertzfeer.blog.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import top.hertzfeer.blog.pojo.Type;
import top.hertzfeer.blog.query.BlogQuery;
import top.hertzfeer.blog.service.BlogService;
import top.hertzfeer.blog.service.TypeService;

import java.util.List;

/**
 * @author by Hertz
 * @Classname TypeController
 * @Description TODO
 * @Date 2020/2/24 15:54
 */
@Controller
public class TypeController {

    @Autowired
    private TypeService typeServiceImpl;

    @Autowired
    private BlogService blogServiceImpl;

    @GetMapping("/types/{id}")
    public String toTypes(@PathVariable Long id,
                          @PageableDefault(size = 8,sort = {"updateTime"},direction = Sort.Direction.DESC)Pageable pageable,
                          Model model){
        List<Type> listTypeTop = typeServiceImpl.listTypeTop(100000);
        if(listTypeTop.size()==0){
            listTypeTop.add(null);
            model.addAttribute("type",listTypeTop);
            model.addAttribute("page",null);
            return "types";
        }
        if(id == -1){
            id = listTypeTop.get(0).getId();
        }
        //查询条件BlogQuery设置
        BlogQuery blogQuery = new BlogQuery();
        blogQuery.setTypeId(id);
        //添加到model
        model.addAttribute("types",listTypeTop);
        model.addAttribute("page",blogServiceImpl.listBlog(pageable,blogQuery));
        model.addAttribute("activeTypeId",id);
        return "types";
    }

}
