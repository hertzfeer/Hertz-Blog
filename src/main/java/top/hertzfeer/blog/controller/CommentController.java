package top.hertzfeer.blog.controller;

import com.google.code.kaptcha.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import top.hertzfeer.blog.pojo.Blog;
import top.hertzfeer.blog.pojo.Comment;
import top.hertzfeer.blog.service.BlogService;
import top.hertzfeer.blog.service.CommentService;
import top.hertzfeer.blog.service.impl.MailService;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author by Hertz
 * @Classname CommentController
 * @Description TODO
 * @Date 2020/2/22 19:31
 */
@Controller
public class CommentController {

    private static List<String> nameList = new ArrayList<>();

    private static String URL_PREFIX = "https://i.picsum.photos/id/";

    private static String URL_SUFFIX = "/200/200.jpg";

    static {
        nameList.add("Hertz-feer");
        nameList.add("hertz");
        nameList.add("hzzz");
        nameList.add("huangzhuang");
        nameList.add("hertz-feer");

    }
    ;

    private ExecutorService executorService = Executors.newFixedThreadPool(2);

    @Autowired
    private CommentService commentServiceImpl;
    @Autowired
    private BlogService blogServiceImpl;
    @Autowired
    private MailService mailService;
    private Logger log = LoggerFactory.getLogger(this.getClass());



    public static void main(String[] args) {
        Random r = new Random();
        System.out.println(r);
        int i = r.nextInt(1100);
        System.out.println(i);
        System.out.println(URL_PREFIX + 1 + URL_SUFFIX);
        HashMap<String, Object> map = new HashMap<>();
        for (int s = 0; s < 12; s++) {
            map.put(String.valueOf(s), i);
        }
        map.put("15", "666");
    }

    @GetMapping("/comments/{blogid}")
    public String comments(@PathVariable("blogid") Long blogid,
                           Model model) {
        model.addAttribute("comments", commentServiceImpl.listCommentByBlogId(blogid));
        return "blog :: commentList";
    }

    @PostMapping("/comments")
    public String post(Comment comment, String verification, HttpServletRequest request) {
        String cache = (String) request.getSession().getAttribute(Constants.KAPTCHA_SESSION_KEY);
        if (!cache.equals(verification)) {
            request.getSession().removeAttribute(Constants.KAPTCHA_SESSION_KEY);
            return "redirect:/comments/" + comment.getBlog().getId();
        }
        request.getSession().removeAttribute(Constants.KAPTCHA_SESSION_KEY);
        Random r = new Random();
        int i = r.nextInt(1100);
        Long blogId = comment.getBlog().getId();
        Blog blog = blogServiceImpl.getBlog(blogId);
        if (comment.getParentComment().getId() != -1) {
            Comment parent = commentServiceImpl.getCommentById(comment.getParentComment().getId());
            if (parent.isAbleEail()) {
                //发送邮件相关的代码
                mailService.doSend(comment,parent.getEmail());
                //邮件相关代码结束
            }
        }
        comment.setBlog(blog);
        if (nameList.contains(comment.getNickName())) {
            comment.setAdminComment(true);
            comment.setAvatar("/images/portrait.jpg");
        } else {
            comment.setAvatar(URL_PREFIX + i + URL_SUFFIX);
        }
        commentServiceImpl.saveComment(comment);
        return "redirect:/comments/" + comment.getBlog().getId();
    }


}
