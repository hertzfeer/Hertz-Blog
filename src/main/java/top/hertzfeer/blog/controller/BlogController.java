package top.hertzfeer.blog.controller;

import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import top.hertzfeer.blog.service.BlogService;
import top.hertzfeer.blog.service.CommentService;

/**
 * @author by Hertz
 * @Classname BlogController
 * @Description TODO
 * @Date 2020/2/21 18:39
 */
@Controller
public class BlogController {

    @Autowired
    private BlogService blogServiceImpl;
    @Autowired
    private CommentService commentServiceImpl;

    @GetMapping("/blog/{id}")
    public String toBlog(@PathVariable Long id, Model model) throws NotFoundException {
        model.addAttribute("blog",blogServiceImpl.getAndConvert(id));
//        model.addAttribute("comments",commentServiceImpl.listCommentByBlogId(id));
        return "blog";
    }

    @GetMapping("/footer/newblog")
    public String newBlogs(Model model){
        model.addAttribute("newblogs",blogServiceImpl.listRecommendBlogTop(3));
        return "_fragments :: newblogList";
    }

    @GetMapping("/error/footer/newblog")
    public String errorNewBlogs(Model model){
        model.addAttribute("newblogs",blogServiceImpl.listRecommendBlogTop(3));
        return "error/_fragments :: newblogList";
    }
}
