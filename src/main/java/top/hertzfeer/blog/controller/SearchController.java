package top.hertzfeer.blog.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import top.hertzfeer.blog.service.BlogService;

/**
 * @author by Hertz
 * @Classname SearchController
 * @Description TODO
 * @Date 2020/2/21 17:20
 */
@Controller
public class SearchController {

    @Autowired
    private BlogService blogServiceImpl;

    @GetMapping("/search")
    public String search(@PageableDefault(size = 8,sort = {"updateTime"} ,direction = Sort.Direction.DESC) Pageable pageable,
                         @RequestParam String query,
                         Model model){
        model.addAttribute("page",blogServiceImpl.listBlog("%"+query+"%",pageable));
        model.addAttribute("query",query);
        return "search";
    }
}
