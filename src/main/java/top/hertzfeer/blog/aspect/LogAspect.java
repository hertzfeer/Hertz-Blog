package top.hertzfeer.blog.aspect;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

/**
 * @author Hertz-feer
 * @date 2020/2/10
 */
@Aspect
@Component
public class LogAspect {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    public static void main(String[] args) {
        HashMap<String, Object> s = new HashMap<>();
        s.put("1", "222");
        System.out.println(1 << 30);
    }

    /**
     * 这是以一个只想controller层所有方法的切面
     */
    @Pointcut("execution(* top.hertzfeer.blog.controller..*.*(..))")
    public void log() {
    }

    @After("log()")
    public void doBefor(JoinPoint joinPoint) {
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest httpServletRequest = servletRequestAttributes.getRequest();
        String url = httpServletRequest.getRequestURL().toString();
        String ip = httpServletRequest.getRemoteAddr();
        String classMethod = joinPoint.getSignature().getDeclaringTypeName() + "." + joinPoint.getSignature().getName();
        Object[] args = joinPoint.getArgs();
        RequestLog requestLog = new RequestLog(url, ip, classMethod, args);
        logger.info("Request:" + requestLog.toString());
    }

    @AfterReturning(pointcut = "log()", returning = "result")
    public void afterResult(Object result) {
        if (result == null) {
            logger.info("生成了一个验证码");
        } else {
            logger.info("Result：" + result.toString());
        }
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @Accessors(chain = true)    //链式写法
    private class RequestLog {
        private String url;
        private String ip;
        private String classMethod;
        private Object[] args;

    }

}
