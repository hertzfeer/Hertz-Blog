package top.hertzfeer.blog.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author by Hertz
 * @Classname User
 * @Description 用户实体类
 * @Date 2020/2/12 18:57
 */
@ToString(exclude = {"blogs"})
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "t_user")
public class User {

    @Id
    @GeneratedValue
    private Long id;
    private String nickName;
    private String username;
    private String password;
    private String email;
    private String avatar;
    private Integer type;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createTime;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateTime;
    /**
     * 一个User对应多个blog
     */
    @OneToMany(mappedBy = "user")
    private List<Blog> blogs = new ArrayList<>();

}
