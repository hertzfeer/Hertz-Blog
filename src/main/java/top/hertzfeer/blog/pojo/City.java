package top.hertzfeer.blog.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author by Hertz
 * @Classname City
 * @Description TODO
 * @Date 2020/2/25 17:20
 */
@ToString
@Data
@AllArgsConstructor
@NoArgsConstructor
public class City {

    private String address;

    private Content content;

    private Integer status;

    @ToString
    @lombok.Data
    @AllArgsConstructor
    @NoArgsConstructor
    private static class Content {
        private Address_detail address_detail;

        private String address;

        private Point point;
    }

    @ToString
    @lombok.Data
    @AllArgsConstructor
    @NoArgsConstructor
    private static class Point {
        private String y;

        private String x;
    }

    @ToString
    @lombok.Data
    @AllArgsConstructor
    @NoArgsConstructor
    private static class Address_detail {
        private String province;

        private String city;

        private String district;

        private String street;

        private String street_number;

        private int city_code;
    }
}
