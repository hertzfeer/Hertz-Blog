package top.hertzfeer.blog.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author by Hertz
 * @Classname Comment
 * @Description 评论实体类
 * @Date 2020/2/12 18:52
 */
@ToString(exclude = {"blog","replayComment","parentComment"})
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "t_comment")
public class Comment implements Serializable {

    @Id
    @GeneratedValue
    private Long id;
    private String nickName;
    private String email;
    /**
     * 头像
     */
    private String avatar;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createTime;

    private String content;

    private boolean adminComment;

    @ManyToOne
    private Blog blog;

    @OneToMany(mappedBy = "parentComment")
    private List<Comment> replyComments = new ArrayList<>();

    @ManyToOne
    private Comment parentComment;

    private boolean ableEail ;
}
