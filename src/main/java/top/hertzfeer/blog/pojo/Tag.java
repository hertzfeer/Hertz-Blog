package top.hertzfeer.blog.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author by Hertz
 * @Classname 标签实体类
 * @Description TODO
 * @Date 2020/2/12 17:35
 */
@ToString(exclude = {"blogs"})
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "t_tag")
public class Tag implements Serializable {

    @Id
    @GeneratedValue
    private Long id;
    private String name;
    /**
     * 多个标签对应多个博客
     */
    @ManyToMany(mappedBy = "tags")
    private List<Blog> blogs = new ArrayList<>();

    public static final String listToString(List<Tag> tags){
        StringBuilder str = new StringBuilder();
        boolean flag = false;
        for (Tag tag: tags) {
            if(flag ){
                str.append(",");
            }else{
                flag = true;
            }
            str.append(tag.getId());
        }
        return str.toString();
    }

    public static void main(String[] args) {
        List<Tag> list = new ArrayList<>();
        Tag tag1 = new Tag(1L,"String",null);
        Tag tag2= new Tag(2L,"String",null);
        Tag tag3 = new Tag(3L,"String",null);
        Tag tag4 = new Tag(4L,"String",null);
        list.add(tag1);
        list.add(tag2);
        list.add(tag3);
        list.add(tag4);
        String str = listToString(list);
        System.out.println(str);

    }
}

