package top.hertzfeer.blog.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author by Hertz
 * @Classname Type
 * @Description 博客类型实体类
 * @Date 2020/2/12 17:33
 */
@ToString(exclude = {"blogs"})
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "t_type")
public class Type implements Serializable {

    @Id
    @GeneratedValue
    private Long id;
    private String name;
    /**
     * 一个类型下有多个博客
     */
    @OneToMany(mappedBy = "type")
    private List<Blog> blogs = new ArrayList<>();

}
