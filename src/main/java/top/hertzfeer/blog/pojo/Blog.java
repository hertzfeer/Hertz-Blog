package top.hertzfeer.blog.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @Classname Blog
 * @Description 博客类容实体类
 * @Date 2020/2/12 16:16
 * @author  by Hertz
 */
@ToString(exclude = {"type","tags","user","comments"})
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="t_blog")
public class Blog implements Serializable {

    @Id
    @GeneratedValue
    private Long id;
    /**
     * 标题
     */
    private String title;
    /**
     * 内容
     */
    @Basic(fetch = FetchType.LAZY)
    @Lob
    private String content;
    /**
     * 首图
     */
    private String firstPicture;
    /**
     * 标识
     */
    private String flag;
    /**
     *  被查看次数
     */
    private Integer views;
    /**
     * 允许赞赏
     */
    private Boolean appreciation;
    /**
     *  允许分享
     */
    private Boolean shareStatement;
    /**
     *  允许评论
     */
    private Boolean commentAble;
    /**
     * 是否发布
     */
    private Boolean published;
    /**
     * 是否推荐
     */
    private Boolean recommend;
    /**
     * 创建时间
     */
    @Temporal(TemporalType.TIMESTAMP)
    private Date createTime;
    /**
     * 修改时间
     */
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateTime;
    /**
     * 博客描述
     */
    private String description;
    /**
     *  一个博客只有一个类型 多个博客享用同一个类型
     */
    @ManyToOne
    private Type type;
    /**
     *  一个博客可能有多个标签
     */
    @ManyToMany(cascade = {CascadeType.PERSIST})
    private List<Tag> tags;

    @ManyToOne
    private User user;

    @OneToMany(mappedBy = "blog")
    private List<Comment> comments;

}
