package top.hertzfeer.blog.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

/**
 * @author by Hertz
 * @Classname Weather
 * @Description TODO
 * @Date 2020/2/25 13:47
 */
@ToString
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Weather {

    private Data data;

    private Integer status;

    private String desc;


    @ToString
    @lombok.Data
    @AllArgsConstructor
    @NoArgsConstructor
    private static class Yesterday {
        private String date;

        private String high;

        private String fx;

        private String low;

        private String fl;

        private String type;
    }
    /**
     * 每一天的天气
     */
    @ToString
    @lombok.Data
    @AllArgsConstructor
    @NoArgsConstructor
    private static class Forecast{
        private String date;

        private String high;

        private String fengli;

        private String low;

        private String fengxiang;

        private String type;
    }

    @ToString
    @lombok.Data
    @AllArgsConstructor
    @NoArgsConstructor
    private static class Data {
        private Yesterday yesterday;

        private String city;

        private List<Forecast> forecast = new ArrayList<>();

        private String ganmao;

        private String wendu;
    }
}
