package top.hertzfeer.blog.interceptor;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author by Hertz
 * @Classname LonginInterceptor
 * @Description TODO
 * @Date 2020/2/13 15:01
 */
public class LoginInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response,
                             Object handler) throws Exception {
        Object user = request.getSession().getAttribute("user");
        if(user == null){
            response.sendRedirect("/admin/rollback");
            return false;
        }
        return true;
    }
}
