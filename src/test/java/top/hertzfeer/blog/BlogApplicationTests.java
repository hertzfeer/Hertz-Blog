package top.hertzfeer.blog;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import top.hertzfeer.blog.pojo.Tag;
import top.hertzfeer.blog.pojo.Type;
import top.hertzfeer.blog.pojo.Weather;
import top.hertzfeer.blog.service.BlogService;
import top.hertzfeer.blog.service.TagService;
import top.hertzfeer.blog.service.TypeService;
import top.hertzfeer.blog.service.impl.MailService;
import top.hertzfeer.blog.service.impl.TemplateService;

import java.io.IOException;
import java.util.List;

@SpringBootTest
class BlogApplicationTests {

    @Autowired
    private TagService tagServiceImpl;
    @Autowired
    private TypeService typeServiceImpl;
    @Autowired
    private TemplateService templateService;
    @Autowired
    private BlogService blogServiceImpl;
    @Autowired
    private MailService mailService;

    @Test
    void contextLoads() {
        List<Tag> list = tagServiceImpl.listTagTop(6);
        for(Tag tag:list){
            System.out.println(tag.toString());
        }
    }

    @Test
    void contextLoads2() {
        List<Type> list = typeServiceImpl.listTypeTop(6);
        for(Type type:list){
            System.out.println(type.toString());
        }
    }

    @Test
    void contextLoads3() throws IOException {
        Weather weather = templateService.getWeather("武汉");
        System.out.println(weather);
    }

    @Test
    void contextLoads4() throws IOException {
        System.out.println(blogServiceImpl.count());
    }

    @Test
    void contextLoads5(){
        mailService.sendMail("hertz_url@163.com","test","test111");
    }


}
